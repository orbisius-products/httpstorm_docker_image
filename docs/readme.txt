This is an image for the George Valkov's httpstorm web server created by bat Slavi.ca
https://httpstorm.com

quick video explanation
==========================
https://www.loom.com/share/92eba4de34eb44fdb358ed3a43e48051

This must be run on Linux or you'll need to adjust the
The current user must be added to the Docker group.
You need to build the image when you first use this and when you make any changes.
You'll need to stop and start the container.

Build
=============
To build the image go to image/ and run the build script.
./build.sh

Updates
=============
When pushing a new release edit/update/replace the files in the image/files/ and build again.

Run
=============
To run this in to the root folder and run via from the command line.
./ctrl.sh up

ctrl+c to stop it.

To delete the network do this.
./ctrl.sh down

This will run it in background.
./ctrl.sh

The ports are specified in the ctrl_env.sh
The server opens port 8080

HTML content
=============

The html content is in site/htdocs
