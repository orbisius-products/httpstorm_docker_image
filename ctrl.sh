#!/bin/bash

# Mark variables and function which are modified or created for export to the environment of subsequent commands
set -a

# set env otherwise they'd be lost
# https://stackoverflow.com/questions/496702/can-a-shell-script-set-environment-variables-of-the-calling-shell
. $(dirname $(realpath "$0"))/ctrl_env.sh

export APP_CUR_SCRIPT_FILE=$(realpath "$BASH_SOURCE")
export APP_CUR_SCRIPT_DIR=$(dirname "$APP_CUR_SCRIPT_FILE")

if [ "$1" == "" ]; then
  export APP_CMD="up -d"
fi

if [ "$1" == "upgrade" ]; then
  echo "Down"
  time docker-compose -f "${APP_CUR_SCRIPT_DIR}/docker-compose.yml" down
  echo "Up"
  time docker-compose -f "${APP_CUR_SCRIPT_DIR}/docker-compose.yml" up -d
elif [[ "$1" == "up" ]] ; then
  time docker-compose -f "${APP_CUR_SCRIPT_DIR}/docker-compose.yml" up
elif [[ "$1" == "start" ]] ; then
  time docker-compose -f "${APP_CUR_SCRIPT_DIR}/docker-compose.yml" up -d
elif [[ "$1" == "down" || "$1" == "stop" ]] ; then
    time docker-compose -f "${APP_CUR_SCRIPT_DIR}/docker-compose.yml" down
else
  time docker-compose -f "${APP_CUR_SCRIPT_DIR}/docker-compose.yml" $APP_CMD
fi
