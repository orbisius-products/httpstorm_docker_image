#!/bin/bash

# usage: build 1.2.3
# usage: build 1.0.0 1 -> to export to tar
# Mark variables and function which are modified or created for export to the environment of subsequent commands
set -a

# set env otherwise they'd be lost
# https://stackoverflow.com/questions/496702/can-a-shell-script-set-environment-variables-of-the-calling-shell
. $(dirname $(realpath "$0"))/../ctrl_env.sh
APP_IMAGE_VER="latest"

if [ "$1" != "" ]; then
  APP_IMAGE_VER="$1"
  APP_IMAGE_VER=$(echo APP_IMAGE_VER | sed "s/[^[:alpha:][:digit:]]/_/g" | awk '{print tolower($0)}' | sed "s/^_*//g"  | sed "s/_*$//g" | sed -E "s/\_+/_/g")
fi

APP_IMAGE_NAME="${APP_HTTPSTORM_DOCKER_IMAGE_NAME}:${APP_IMAGE_VER}"

echo "APP_PROJECT_NAME: $APP_PROJECT_NAME"
echo "Current User Owner: $APP_CURRENT_USER ($APP_CURRENT_UID)"
echo "Image ID: $APP_IMAGE_NAME"

time docker build \
  --build-arg APP_CURRENT_USER="$APP_CURRENT_USER" \
  --build-arg APP_CURRENT_UID="$APP_CURRENT_UID" \
  --build-arg APP_IMAGE_NAME="$APP_IMAGE_NAME" \
  -t "$APP_IMAGE_NAME" .

if [ "$2" != "" ]; then
  echo "Exporting image"
  time docker image save -o "$APP_IMAGE_NAME.tar" "$APP_IMAGE_NAME"
fi
